# typing_practice

打字游戏，将所有单词消灭即赢，当单词碰到底部，即输。纯bootstrap3、css3、jquery、html实现。

演示：
https://lee1989.gitee.io/typing_practice

## 截图
![截图1](https://gitee.com/lee1989/typing_practice/raw/master/1.gif)
